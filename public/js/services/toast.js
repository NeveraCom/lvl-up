toast = {
  toastBlock: document.getElementById('toast'),
  toastText: document.getElementById('toast-text'),
  // show: false,

  /**
   * Тимчасове відображення toast
   * 
   * @param {string} message 
   * @param {string} state
   * @param {number} time 
   */
  show(message = 'Unknown message', state = 'primary', time = 2500) {
    this.toastBlock.classList.remove('toast_warning', 'toast_primary');
    this.toastText.innerText = message;

    switch (state) {
      case 'warning': 
        this.toastBlock.classList.add('toast_warning'); 
        break;

      case 'primary':
      default:
        this.toastBlock.classList.add('toast_primary');
        break;
    }

    this.toastBlock.classList.add('toast_show');
    setTimeout(() => this.hide(), time);
  },

  /**
   * hiding toast
   */
  hide() {
    this.toastBlock.classList.remove('toast_show');
  },
};

document.getElementById('toast-close-button').addEventListener('click', () => toast.hide());
