const common = {
  getVariants(word, type = 'toEn', num = 4) {
    // add actual words
    const words = library.filter(e => !e.ignored);
    const res = [];

    // fill res array
    for (let i = 0; i < num; i++) {
      const number = this.getRandom(0, words.length - 1);
      res.push(getWordShort(words[number], type));

      // remove used word from wordlist
      words.splice(number, 1);
    }

    // insert right answer
    if (!res.some(e => e.id === word.english)) {
      // replace random word by right
      res.splice(this.getRandom(0, num - 1), 1, getWordShort(word, type));
    }

    return res;

    function getWordShort(word, direction) {
      const transcription = word.transcription ? ` <b>[${word.transcription}]</b>` : '';

      return direction === 'toEn'
              ? { id: word.english, value: word.english + transcription }
              : { id: word.english, value: word.native };
    }
  },
  
  getRandom(min = 0, max = 1) {
    return Math.floor(Math.random() * (max - min + 1) + min);
  },

  getLetters(word) {
    // translate string 'abc' to array ['a', 'b', 'c'] and add extra letters
    return this.shuffle([...word.english.split(''), ...this.generateRandomLetters().split('')]);
  },

  generateRandomLetters() {
    const length = current.settings.constructorAddLetters.state ? current.settings.constructorAddLetters.current : 0;
    let result = '';
    const characters = 'abcdefghijklmnopqrstuvwxyz';
    const charactersLength = characters.length;
    for ( let i = 0; i < length; i++ ) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
  },

  shuffle(letters) {
    for (let i = letters.length - 1; i > 0; i--) {
      let j = Math.floor(Math.random() * (i + 1));
      [letters[i], letters[j]] = [letters[j], letters[i]];
    }

    return letters;
  },

  /**
   * show stages as point. current stage is bold
   *
   * @param {number} currentStage
   * @param {number} totalStages
   * @returns {string}
   */
  getStageTemplate(currentStage, totalStages) {
    let items = '';
    for (let i = 0; i < totalStages; i++) {
      items += `<div class="stage__item ${i === currentStage ? ' stage__item_full' : ''}"></div>`;
    }

    return `<div class="stage">${items}</div>`;
  },
}
