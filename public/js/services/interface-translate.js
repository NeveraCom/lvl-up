function translate(str) {
  const param = str.split('.');
  let result = '';

  switch (param.length) {
    case 1: result = lang[param[0]] && lang[param[0]][current.lang]; break
    case 2: result = lang[param[0]] && lang[param[0]][param[1]] && lang[param[0]][param[1]][current.lang]; break
  }

  return result || '___';
}

const lang = {
  start: { ua: 'Вчити', en: 'learn' },
  blitz: { ua: 'Повторяти', en: 'repeat' },
  settings: { ua: 'налаштування', en: 'settings' },
  myWords: { ua: 'мої слова', en: 'my words' },

  all: { ua: 'Всі', en: 'All' },
  new: { ua: 'Нові', en: 'New' },
  learned: { ua: 'Вивчені', en: 'Learned' },
  ignored: { ua: 'Игноровані', en: 'Ignored' },
  search: { ua: 'Пошук', en: 'Search' },

  newOne: { ua: 'Новe', en: 'New' },
  learnedOne: { ua: 'Вивченe', en: 'Learned' },
  ignoredOne: { ua: 'Игнорується', en: 'Ignored' },

  enterWord: { ua: 'Введіть слово...', en: 'Enter word...' },
  enterTranscription: { ua: 'Транскрипція (не обов\'язково)...', en: 'Transcription (not necessary)...' },
  enterTranslate: { ua: 'Введіть переклад...', en: 'Enter translate...' },

  deleteWordMessage: { ua: 'Видалити це слово зі словника?', en: 'Remove this word from the dictionary?' },

  // buttons
  button_copy: { ua: 'Скопіювати', en: 'copy' },
  button_clear: { ua: 'Очистити', en: 'clear' },

  clearDB: { ua: 'Видалити тимчасову базу даних?', en: 'Delete temporary database?' },
  DBClearedSuccess: { ua: 'Базу даних успішно очищено', en: 'The database was successfully cleared' },
  DBClearedError: { ua: 'Під час спроби видалити базу даних сталася помилка', en: 'An error occurred while trying to delete the database' },

  DBCopiedSuccess: { ua: 'Базу даних успішно скопіойвано', en: 'Database copied successfully' },
  DBCopiedError: { ua: 'Під час спроби скопіювати базу даних сталася помилка', en: 'An error occurred while attempting to copy the database' },

  // home
  home_start_error_1: { ua: 'Замало слів для вивчання (треба від 4)', en: 'Not enough words to learn (need 4)' },
  home_start_error_2: { ua: 'Замало слів для повторювання (треба від 8)', en: 'Not enough words to repeat (need 8)' },

  BUTTONS: {
    DELETE_ALL: { ua: 'Видалити все', en: 'Delete all'},
  },
  SETTINGS: {
    HEADER_MAIN: { ua: 'Налаштування', en: 'Settings'},
    HEADER_COMMON: { ua: 'Загальні', en: 'Common' },
    HEADER_LEARN: { ua: 'Вивчення', en: 'Learn' },
    HEADER_REPEAT: { ua: 'Повторення', en: 'Repeat' },
    LEARN_WORDS: { ua: 'Стартова кількість слів для навчання', en: 'Starting number of words for learning' },
    REPEAT_WORDS: { ua: 'Кількість слів для повторення', en: 'Number of words to repeat' },
    OPTION_LEARN_STAGE_TO_NATIVE: { ua: 'Переклад на рідну', en: 'Translation into native language' },
    OPTION_LEARN_STAGE_TO_ENGLISH: { ua: 'Переклад на англійську', en: 'Translation into English' },
    OPTION_LEARN_STAGE_CONSTRUCTOR: { ua: 'Конструктор', en: 'Constructor' },
    OPTION_LEARN_STAGE_WRITING: { ua: 'Написання', en: 'Writing' },

    OPTION_LEARN_CONSTRUCTOR_ADD_LETTERS: { ua: 'Додавати зайві літери в конструкторі', en: 'Add extra letters in the constructor' },
    OPTION_LEARN_CONSTRUCTOR_ADD_LETTERS_COUNT: { ua: 'Кількість зайвих букв', en: 'Number of extra letters' },

    HEADER_SAVING: { ua: 'Збереження', en: 'Saving' },
    HOW_SAVING: { ua: 'Як цей застосунок зберігає словник', en: 'How this application stores the dictionary' },
    ABOUT_CLEARING_DB: { ua: 'Що можна видалити', en: 'What can be removed' },
  },
  SETTINGS_MESSAGE: {
    DELETE_ALL: { ua: 'Видалити всі налаштування і тимчасову базу слів', en: 'Delete all settings and temporary word base' },
  },
  ERROR_MESSAGES: {
    WRONG_VERSION: {
      ua: 'Виявлено, що поточна версія застосунка відрізняється від версії налаштувань. Задля запобігання критичних помилок налаштування (не словникова база) будуть видалені.',
      en: 'It was detected that the current version of the application differs from the version of the settings. To prevent critical errors, the settings (not the dictionary base) will be removed.'
    },
  },

  settings_text1: { 
    ua: 'Застосунок працює виключно на пристрої, це вносить деякі обмеження для зберігання бази слів (щонайменш для поточної версії). Пеший набір даних зберігається у файлі <b>/db/words.js</b>, другий, який має перевагу, у локальному сховищі (localStorage). Тобто застосунок бере слова із файла, та зверху накатує слова із локального сховища.', 
    en: 'The app works only on the device, which imposes some restrictions on wordbase storage (at least for the current version). The pedestrian data set is stored in the file <b>/db/words.js</b>, the second, which has priority, in the local storage (localStorage). That is, the application takes words from the file, and adding and rewrite words from the local storage on top.' 
  },
  settings_text2: { 
    ua: 'Для перенесення тимчасового словника до файлу (наприклад для оновлення, переносу на інший пристрій або папку на поточному пристрої), натисніть кнопку "Скопіювати", та із заміною вставте вміст скопійованого до файлу <b>/db/words.js</b>', 
    en: 'To move a temporary dictionary to a file (for example, to update, move to another device, or a folder of current device), click the "Copy" button and paste with replace the contents of the copied into the file <b>/db/words.js</b>' 
  },
  settings_text3: { 
    ua: 'Можно почистити локальне сховище натиснув кнопку "Очистити". Але при змінах актуальна база знов буде збережена.', 
    en: 'You can clear the local storage by clicking the "Clear" button. But with changes, the current database will be saved again.' 
  },

  // select words
  select_words_header: { ua: 'Оберіть слова для вивчення та їх кількість', en: 'Choose the words to study and their quantity' },
  select_words_message_1: { ua: 'Нема больше слів для вивчання, щоб замінити', en: 'No more learning words to replace' },
  select_words_message_2: { ua: 'Ви не можете вивчати більше слів одночасно', en: 'You cannot learn more words at the same time' },
  select_words_message_3: { ua: 'Нема больше слів для вивчання', en: 'No more learning words' },
  select_words_message_4: { ua: 'Не можна вивчати одночасно менше слів', en: 'You cannot learn fewer words at the same time' },

  // translate words
  translate_words_header: { ua: 'Оберіть слово - переклад', en: 'Choose a word - translation' },
  translate_words_exit: { ua: 'Закінчити тренування?', en: 'Finish training?' },

  // word-constructor_header
  word_constructor_header: { ua: 'Складіть слово з букв', en: 'Make a word from the letters' },

  // word write header
  word_write_header: { ua: 'Введіть переклад', en: 'Enter the translation' },

  // results
  results_header: { ua: 'Результати', en: 'Results' }
}
