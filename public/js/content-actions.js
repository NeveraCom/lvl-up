
content.addEventListener('click', (e) => { 
  // CHECKBOXS
  const checkboxId = e.target.closest('.js-checkbox')?.getAttribute('id');

  if (checkboxId) {
    switch (current.page) {
      case  'home': home.actions(checkboxId, null); break;
      case  'settings': settings.actions(checkboxId, null); break;
    }
  }

  // BUTTON-WORD
  const btn = e.target.closest('.js-button');
  const btnWord = e.target.closest('.js-button-word');

  if (btnWord) {
    const value = btnWord.getAttribute('data-value');
    if (!value) return;

    switch (current.page) {
      case 'words': modal.displayModal(value); break;
      case 'selectWords': selectWords.changeWord(value); break;
      case 'translateWords': translateWords.showRightAnswer(value); break;
      case 'learnResult': modal.displayModal(value); break;
      case 'wordConstructor': wordConstructor.selectLetter(value); break;
    }
  }

  // BUTTON
  if (btn) {
    const id = btn.getAttribute('id');
    if (!id) return;

    switch (current.page) {
      // case current.modalOpen: modal.actions(id, btn.getAttribute('data-value') || null); break;
      case 'words': words.actions(id, btn.getAttribute('data-value') || null); break;
      case 'home': home.actions(id, btn.getAttribute('data-value') || null); break;
      case 'settings': settings.actions(id, btn.getAttribute('data-value') || null); break;
      case 'selectWords': selectWords.actions(id); break;
      case 'translateWords': translateWords.actions(id); break;
      case 'learnResult': learnResult.actions(id); break;
      case 'wordConstructor': wordConstructor.actions(id); break;
      case 'wordWrite': wordWrite.actions(id); break;
    }
  }
});


content.addEventListener('keyup', (e) => {
  // FIELD
  const field = e.target.closest('.js-field');
  const id = field?.getAttribute('id');

  if (field && id) {
    switch (current.page) {
      case 'words': words.actions(id, field.value); break;
      case 'wordWrite': wordWrite.actions(id, field.value); break;
    }
  }
});
