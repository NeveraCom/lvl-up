const home = {
  wordsForLearn: 0,
  wordsForRepeat: 0,

  displayPage() {
    current.page = 'home';
    this.wordsForLearn = this.getQuantityWordsForLearn();
    this.wordsForRepeat = this.getQuantityWordsForRepeat();
    content.innerHTML = this.getTemplate();
  },

  getQuantityWordsForLearn() {
    return library.filter(e => !e.learnedAt).length;
  },

  getQuantityWordsForRepeat() {
    return library.filter(e => e.learnedAt).length;
  },

  getTemplate() {
    return `
      <div class="button-group">
        <button class="button js-button" id="learn" data-value="learn">${lang.start[current.lang]} (${this.wordsForLearn})</button>
        <button class="button js-button" id="repeat" data-value="repeat">${lang.blitz[current.lang]} (${this.wordsForRepeat})</button>
      </div>

      <div class="content__home-actions">
        <button class="button button_icon button_large both-mode js-button" id="settings" title="${lang.settings[current.lang]}">${iconList.options}</button>
        <button class="button button_icon button_large both-mode js-button" id="words" title="${lang.myWords[current.lang]}">${iconList.book}</button>
      </div>
    `;
  },

  actions(id, value = '') {
    switch(id) {
      case 'settings':
        settings.displayPage();
        break;
      case 'words':
        words.displayPage();
        break;
      case 'learn':
        if (this.wordsForLearn < current.settings.learnWords.current) {
          toast.show(lang.home_start_error_1[current.lang], 'warning');
        } else {
          learn.start('learn');
        }
        break;
      case 'repeat':
        if (this.wordsForRepeat < current.settings.repeatWords.current) {
          toast.show(lang.home_start_error_2[current.lang], 'warning');
        } else {
          learn.start('repeat');
        }
        break;
    }
  },
}

/**
 * START APP
 */
current.start();
