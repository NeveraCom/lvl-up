const words = {
  filter: 'all', // 'all' 'new', 'ignored', 'learned'
  sort: 'a', // 'a', 'z', 'new', 'old'
  search: '',
  selectedWords: [],
  collapsed: true,

  filterList: [
    { id: 'filter-all', value: 'all' },
    { id: 'filter-new', value: 'new' },
    { id: 'filter-learned', value: 'learned' },
    { id: 'filter-ignored', value: 'ignored' }
  ],

  sortList: [
    { id: 'sort-a', value: 'a', icon: 'sortA' },
    { id: 'sort-z', value: 'z', icon: 'sortZ' },
    { id: 'sort-new', value: 'new', icon: 'sortNew' },
    { id: 'sort-old', value: 'old', icon: 'sortOld' }
  ],

  useSearch() {
    this.selectedWords = this.search ? library.filter(w => w.english.includes(this.search)) : library;
    
    return this;
  },

  useFilter() {
    // let selectedWords = [];

    switch (this.filter) {
      case 'all': 
        return this;
        // this.selectedWords = this.selectedWords;
        break;
      case 'ignored':
        this.selectedWords = this.selectedWords.filter(w => w.ignored);
        break;
      case 'learned':
        this.selectedWords = this.selectedWords.filter(w => w.learnedAt);
        break;
      case 'new':
        this.selectedWords = this.selectedWords.filter(w => !w.learnedAt && !w.ignored);
        break;
    }

    return this;
  },

  useSort() {
    switch (this.sort) {
      case 'a': this.selectedWords.sort(useSortA); break;
      case 'z': this.selectedWords.sort(useSortZ); break;
      case 'new': this.selectedWords.sort(useSortDateNew); break;
      case 'old': this.selectedWords.sort(useSortDateOld); break;
    }

    function useSortA(a, b) {
      return a.english.toLowerCase() > b.english.toLowerCase() ? 0 : -1;
    }
  
    function useSortZ(a, b) {
      return a.english.toLowerCase() > b.english.toLowerCase() ? -1 : 0;
    }
  
    function useSortDateNew(a, b) {
      return a.createdAt > b.createdAt ? -1 : 0;
    }
  
    function useSortDateOld(a, b) {
      return a.createdAt > b.createdAt ? 0 : -1;
    }
  }, 

  displayPage() {
    // create page
    content.innerHTML = this.getTemplate();

    this.displayWords();
  },

  displayWords() {
    current.page = 'words';

    // select words
    this.useSearch().useFilter().useSort();

    // create templates for words
    let wordTemplates = '';

    for(let word of this.selectedWords) {
      wordTemplates += this.getWordTemplate(word);
    }

    document.querySelector('.content__list').innerHTML = wordTemplates;
  },

  getWordTemplate(word) {
    const iconStatus = word.ignored ? iconList.wordIgnored : word.learnedAt ? iconList.wordReady : iconList.wordNew;
    const wordTranscription = word.transcription ? ` <b>[${word.transcription}]</b>` : '';

    return `
      <button class="button button_word-full js-button-word" data-value="${word?.english}">
        <div class="button__caption">
          ${word?.english || '-'}
          ${wordTranscription}
        </div>
        <div class="button__value">${word?.native || '-'}</div>
        <span class="button__icon-wrapper">${iconStatus}</span> 
      </button>
    `;
  },

  getTemplate() {
    const filterButtons = this.filterList.reduce((sum, e) => {
      return sum + `<button class="button button_small ${this.filter === e.value ? 'button_active' : ''} js-button" id="${e.id}" data-value="${e.value}">${lang[e.value][current.lang]}</button>`;
    }, '');

    const sortButtons = this.sortList.reduce((sum, e) => {
      return sum + `<button class="button button_icon button_small both-mode ${this.sort === e.value ? 'button_active' : ''} js-button" id="${e.id}" data-value="${e.value}">${iconList[e.icon]}</button>`;
    }, '');

    return `
      <div class="button-group button-group_navigation">
        <button class="button button_icon button_small both-mode button_without-shadow js-button" id="to-home">${iconList.arrowLeft}</button>
        <button class="button button_icon button_small both-mode button_without-shadow js-button" id="words-new">${iconList.plusThink}</button>
      </div>

      <div class="content__collapsed" id="collapsed">
        <div class="button-group button-group_rows" id="words-filters">${filterButtons}</div>

        <div class="button-group button-group_row" id="words-sort">${sortButtons}</div>

        <div class="custom-field__wrapper">
          <input type="text" class="custom-field custom-field_small js-field" placeholder="${lang.search[current.lang]}" id="words-search">
          <button class="button custom-field__delete button_icon button_without-shadow button_x-small js-button" id="words-search-clear">${iconList.delete}</button>
        </div>
      </div>

      <div class="button-group button-group_row button-group_low">
        <button class="button button_icon button_small both-mode button_without-shadow js-button" id="collapse">${iconList.chevronDown}</button>
      </div>

      <div class="content__list"></div>
    `;
  },

  actions(id, value) {
    switch(id) {
      case 'words-search':
        this.search = value;
        this.displayWords();
        break;
      case 'words-search-clear':
        this.search = '';
        document.getElementById('words-search').value = '';
        this.displayWords();
        break;

      case 'sort-a':
      case 'sort-z':
      case 'sort-new':
      case 'sort-old': 
        this.sortUpdate(id, value);
        break;

      case 'filter-all':
      case 'filter-new':
      case 'filter-learned':
      case 'filter-ignored':
        this.filterUpdate(id, value);
        break;

      case 'collapse':
        this.collapseUpdate();
        break;

      case 'to-home': 
        home.displayPage();
        break;

      case 'words-new':
        modal.displayModal();
        break;
    }
  },

  sortUpdate(id, value) {
    const group = document.getElementById('words-sort');
    group.querySelectorAll('.button').forEach(e => e.classList.remove('button_active'));
    document.getElementById(id).classList.add('button_active');

    this.sort = value;
    this.displayWords();
  },

  filterUpdate(id, value) {
    const group = document.getElementById('words-filters');
    group.querySelectorAll('.button').forEach(e => e.classList.remove('button_active'));
    document.getElementById(id).classList.add('button_active');

    this.filter = value;
    this.displayWords();
  },

  collapseUpdate() {
    this.collapsed = !this.collapsed;
    const cls = document.getElementById('collapsed');
    const btn = document.getElementById('collapse');
    if (this.collapsed) {
      cls.classList.remove('content__collapsed_open');
      btn.classList.remove('button_rotated');
    } else {
      cls.classList.add('content__collapsed_open');
      btn.classList.add('button_rotated');
    } 
  }
}
