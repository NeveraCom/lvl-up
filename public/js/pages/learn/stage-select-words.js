const selectWords = {
  newWords: [],
  currentList: [],
  currentStage: 0,
  totalStages: 0,
  pageName: 'selectWords',

  start(currentStage, totalStages) {
    current.page = this.pageName;
    this.currentStage = currentStage;
    this.totalStages = totalStages;

    // clearing
    this.currentList.length = 0;

    this.newWords = library.filter(w => !w.learnedAt);
    for (let i = 0; i < current.settings.learnWords.current; i++) {
      this.currentList.push(this.newWords.shift());
    }

    this.displayPage();
  },
  
  displayPage() {
    content.innerHTML = this.getTemplate();
    this.displayWords();
  },

  getTemplate() {
    const stages = common.getStageTemplate(this.currentStage, this.totalStages);

    return `
      <div class="content__value">
        <div class="button-group button-group_navigation">
          <button class="button button_icon button_small both-mode button_without-shadow js-button" id="settings-home">${iconList.arrowLeft}</button>
        </div>
      
        ${stages}

        <h2 class="header-2">${lang.select_words_header[current.lang]}</h2> 

        <div class="content__word-list" id="word-list"></div>

        <div class="button-group button-group_row">
          <button class="button button_icon js-button" id="remove-word" data-value="delete">-1</button>
          <button class="button button_icon js-button" id="add-word" data-value="new">+1</button>
          <button class="button button_icon js-button" id="apply" data-value="learned">${iconList.ok}</button>
        </div>
      </div>
    `;
  },

  displayWords() {
    const wordList = this.currentList.reduce((sum, word) => sum + this.getWordTemplate(word), '');
    document.getElementById('word-list').innerHTML = wordList;
  },

  getWordTemplate(word) {
    const wordTranscription = word.transcription ? ` <b>[${word.transcription}]</b>` : '';

    return `
      <button class="button button_word-short js-button-word" data-value="${word?.english}">
        <div class="button__caption">
          ${word?.english}
          ${wordTranscription}
        </div>
          
        <div class="button__value">${word?.native}</div>
      </button>
    `;
  },

  actions(id) {
    switch(id) {
      case 'settings-home':
        home.displayPage();
        break;
      case 'add-word':
        this.addWord();
        break;
      case 'remove-word':
        this.removeWord();
        break;
      case 'apply':
        learn.setWords(this.currentList);
        learn.endStage();
        break;
    }
  },

  changeWord(value) {
    if (this.newWords.length > 0) {
      // remove word from Current to end of total
      // get first word from total to replace value
      const index = this.currentList.map(w => w.english).indexOf(value);
      this.newWords.push(...this.currentList.splice(index, 1, this.newWords.shift()));

      this.displayWords();
      
    } else {
      toast.show(lang.select_words_message_1[current.lang], 'warning')
    }
  },

  addWord() {
    const max = current.settings.learnWords.max;

    if (this.currentList.length < max && this.newWords.length > 0) {
      this.currentList.push(this.newWords.shift());
      this.displayWords();

    } else if (this.currentList.length === max) {
      toast.show(lang.select_words_message_2[current.lang], 'warning');

    } else if (this.newWords.length === 0) {
      toast.show(lang.select_words_message_3[current.lang], 'warning');
    }
  },

  removeWord() {
    const min = current.settings.learnWords.min;

    if (this.currentList.length > min) {
      this.newWords.push(this.currentList.pop());
      this.displayWords();

    } else {
      toast.show(lang.select_words_message_4[current.lang], 'warning');
    }
  },
};
