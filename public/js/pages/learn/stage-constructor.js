const wordConstructor = {
  words: [],
  readyWords: [],
  currentWord: null,
  letters: [],
  selectedLetters: [],
  currentStage: 0,
  totalStages: 0,
  
  /**
   * @param {Object[]} words
   * @param {number} currentStage
   * @param {number} totalStages
   */
  start(words, currentStage, totalStages) {
    current.page = 'wordConstructor';
    this.currentStage = currentStage;
    this.totalStages = totalStages;

    // clearing
    this.readyWords.length = 0;
    this.currentWord = null;
    this.letters.length = 0;

    // set new data
    this.words = words;

    this.nextStep();
  },

  /**
   * start new word or finish stage
   */
  nextStep() {
    if (this.words.length) {
      // erase field
      const field = document.getElementById('entered-word');
      if (field) {
        field.value = '';
      }
      this.selectedLetters.length = 0;

      this.currentWord = this.words[0];
      this.letters = common.getLetters(this.currentWord);
      this.displayPage();
    
    } else {
      learn.endStage(this.readyWords.map(e => {
        // need erase translated field for next translate
        e.translated = false;
        return e;
      }));
    }
  },

  displayPage() {
    content.innerHTML = this.getTemplate();
    this.displayLetters();
  },

  getTemplate() {
    const stages = common.getStageTemplate(this.currentStage, this.totalStages);

    return `
      <div class="content__value">
        <div class="button-group button-group_navigation">
          <button class="button button_icon button_small both-mode button_without-shadow js-button" id="home">
            ${iconList.close1}
          </button>
        </div>

        ${stages}

        <h2 class="header-2">${lang.word_constructor_header[current.lang]}</h2> 

        <div class="word">${this.currentWord.native}</div>
        <div class="content__letter-list content__letter-list_answer" id="answer-letter-list"></div>

        <div class="content__letter-list" id="letter-list"></div>

        <div class="button-group button-group_row" id="actions">
          <button class="button button_icon js-button" id="help">${iconList.question1}</button>
        </div>
      </div>
    `;
  },

  /**
   * display letters for this word
   */
  displayLetters() {
    document.getElementById('letter-list').innerHTML =
      this.letters.reduce((sum, letter, index) => sum + this.getLetterTemplate(letter, index), '');
  },

  /**
   *
   * @param {string} letter
   * @param {number} index
   * @returns {string}
   */
  getLetterTemplate(letter, index) {
    return `
      <button class="button button_icon" onclick="wordConstructor.selectLetter('${letter}', ${index})">
        <div class="button__caption">${letter}</div>
      </button>
    `;
  },

  getAnswerLetterTemplate(letter, index) {
    return `
      <button class="button button_answer" onclick="wordConstructor.removeLetter('${letter}', ${index})">
        <div class="button__caption">${letter}</div>
      </button>
    `;
  },

  actions(id) {
    switch(id) {
      case 'home':
        if (window.confirm(lang.translate_words_exit[current.lang])) {
          home.displayPage();
        }
        break;
      case 'help':
        this.showRightAnswer();
        break;
      case 'next':
        this.nextStep();
        break;
    }
  },

  selectLetter(letter, index) {
    if (!letter) return;

    // tag letter as selected
    document.querySelectorAll('#letter-list .button')[index].setAttribute('disabled', true);

    this.selectedLetters.push(letter);
    this.updateAnswer();

    if (this.isWordRight()) {
      this.showRightAnswer(this.selectedLetters);
    }
  },

  removeLetter(letter, index) {
    // remove from selected and update selected letters
    this.selectedLetters.splice(index, 1);
    this.updateAnswer();
    // back letter to letter's list
    const letters = document.querySelectorAll('#letter-list .button');
    const indexLetter = Array.from(letters)
      .findIndex(e => (
        e.querySelector('.button__caption').innerText.toLowerCase() === letter.toLowerCase()
        && e.hasAttribute('disabled')
      ));

    document.querySelectorAll('#letter-list .button')[indexLetter].removeAttribute('disabled');
  },

  isWordRight() {
    return this.selectedLetters.join('').toLowerCase() === this.currentWord.english.toLowerCase();
  },

  updateAnswer() {
    document.getElementById('answer-letter-list').innerHTML =
      this.selectedLetters.reduce((sum, letter, index) => sum + this.getAnswerLetterTemplate(letter, index), '');
  },

  showRightAnswer(answer = null) {
    if (answer) {
      this.currentWord.translated = true; // update status - no more translate for it
      this.readyWords.push(this.currentWord);
      this.words.shift(); // remove word from words

      document.querySelector('#answer-letter-list').innerHTML = `
        <span class="answer answer_correct">
          ${this.selectedLetters.join('').toUpperCase()}
        </span>`;

      document.querySelector('#letter-list').innerHTML = '';

    } else {
      this.currentWord.hasError = true; // has error for all train
      this.words.shift(); // remove word from start pos
      this.words.push(this.currentWord); // push it to end pos

      document.querySelector('#answer-letter-list').innerHTML = `
        <span class="answer answer_incorrect">
          ${this.currentWord.english.toUpperCase()}
        </span>`;

      document.querySelector('#letter-list').innerHTML = '';
    }

    // change HELP button by NEXT button
    document.getElementById('actions').innerHTML = `<button class="button button_icon js-button" id="next">${iconList.chevronRight}</button>`;
  },
}
