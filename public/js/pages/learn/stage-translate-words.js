const translateWords = {
  words: [],
  readyWords: [],
  currentWord: null,
  direction: '',
  // quantity: 0,
  variants: [],
  currentStage: 0,
  totalStages: 0,
  
  /**
   * @param {string} direction - may have values: toEn / toNative
   * @param {Object[]} words
   * @param {number} currentStage
   * @param {number} totalStages
   */
  start(direction, words, currentStage, totalStages) {
    current.page = 'translateWords';
    this.currentStage = currentStage;
    this.totalStages = totalStages;

    // clearing
    this.readyWords.length = 0;
    this.currentWord = null;
    this.variants.length = 0;

    // set new data
    this.direction = direction;
    this.words = words;

    this.nextStep();
  },

  nextStep() {
    if (this.words.length) {
      this.currentWord = this.words[0];
      this.variants = common.getVariants(this.currentWord, this.direction);
      this.displayPage();
    
    } else {
      learn.endStage(this.readyWords.map(e => {
        // need erase translated field for next translate
        e.translated = false;
        return e;
      }));
    }
  },

  displayPage() {
    content.innerHTML = this.getTemplate();
    this.displayWords();
  },

  getTemplate() {
    const stages = common.getStageTemplate(this.currentStage, this.totalStages);
    let word = '';
    let wordTranscription = '';
    if (this.direction === 'toEn') {
      word = `<b>${this.currentWord.native}</b>`;
      wordTranscription = '';
    } else {
      word = `<b>${this.currentWord.english}</b>`;
      wordTranscription = this.currentWord.transcription ? ` <small>[${this.currentWord.transcription}]</small>` : '';
    }

    return `
      <div class="content__value">
        <div class="button-group button-group_navigation">
          <button class="button button_icon button_small both-mode button_without-shadow js-button" id="home">
            ${iconList.close1}
          </button>
        </div>

        ${stages}

        <h2 class="header-2">${lang.translate_words_header[current.lang]}</h2> 

        <div class="word">${word} ${wordTranscription}</div>
        <div class="content__word-list" id="word-list"></div>

        <div class="button-group button-group_row" id="actions">
          <button class="button button_icon js-button" id="help">${iconList.question1}</button>
        </div>
      </div>
    `;
  },

  displayWords() {
    const wordList = this.variants.reduce((sum, word) => sum + this.getWordTemplate(word), '');
    document.getElementById('word-list').innerHTML = wordList;
  },

  getWordTemplate(word) {
    return `
      <button class="button button_word-simple js-button-word" data-value="${word.id}">
        <div class="button__caption">${word.value}</div>
      </button>
    `;
  },

  showRightAnswer(answer = null) {
    // check answer
    const rightAnswer = this.currentWord.english;
    this.hightLightWord(rightAnswer, true);

    if (answer && answer === rightAnswer) {
      this.currentWord.translated = true; // update status - no more translate for it
      this.readyWords.push(this.currentWord);
      this.words.shift(); // remove word from words
    
    } else {
      if (answer) {
        this.hightLightWord(answer, false);
      }

      this.currentWord.hasError = true; // has error for all train
      this.words.shift(); // remove word from start pos
      this.words.push(this.currentWord); // push it to end pos
    }

    // change HELP button by NEXT button
    document.getElementById('actions').innerHTML = `<button class="button button_icon js-button" id="next">${iconList.chevronRight}</button>`;
    // remove ability to reselect answer
    document.querySelectorAll('#word-list .js-button-word').forEach(e => e.classList.remove('js-button-word'));
  },

  hightLightWord(value, rightAnswer = true) {
    const className = rightAnswer ? 'button_word-simple_right' : 'button_word-simple_wrong';
    document.querySelector(`.js-button-word[data-value=${value}]`).classList.add(className);
  },

  actions(id) {
    switch(id) {
      case 'home':
        if (window.confirm(lang.translate_words_exit[current.lang])) {
          home.displayPage();
        }
        break;
      case 'help':
        this.showRightAnswer();
        break;
      case 'next':
        this.nextStep();
        break;
    }
  },
};
