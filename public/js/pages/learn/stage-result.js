const learnResult = {
  words: [],
  currentStage: 0,
  totalStages: 0,

  start(words, currentStage, totalStages) {
    this.words.length = 0;
    current.page = 'learnResult';
    this.currentStage = currentStage;
    this.totalStages = totalStages;

    for(let word of words) {
      const currentWord = library.find(e => e.english === word.english);
      if (!word.hasError) {

        switch (learn.trainType) {
          case 'learn':
            currentWord.learnedAt = Date.now();
            break;

          case 'repeat':
            currentWord.repeatAt = Date.now();
            break;
        }
      } else {
        switch (learn.trainType) {
          case 'repeat':
            currentWord.repeatAt = null;
            break;
        }
      }
      this.words.push(currentWord);
    }

    this.displayPage();
  },

  displayPage() {
    content.innerHTML = this.getTemplate();
    this.displayWords();
  },

  getTemplate() {
    const stages = common.getStageTemplate(this.currentStage, this.totalStages);

    return `
      <div class="content__value">
        ${stages}
      
        <h2 class="header-2">${lang.results_header[current.lang]}</h2> 

        <div class="content__word-list" id="word-list"></div>

        <div class="button-group button-group_row" id="actions">
          <button class="button button_icon js-button" id="done">${iconList.ok}</button>
        </div>
      </div>
    `;
  },

  updatedDisplayWords() {
    // go to library and update words params
    this.words = this.words.map(e => {
      return library.find(el => el.english === e.english)
    });

    this.displayWords();
  },

  displayWords() {
    document.getElementById('word-list').innerHTML = this.words.reduce((sum, word) => sum + this.getWordTemplate(word), '');
  },

  getWordTemplate(word) {
    let iconStatus = iconList.question1;
    if (learn.trainType === 'learn') iconStatus = word.ignored ? iconList.wordIgnored : word.learnedAt ? iconList.wordReady : iconList.wordError;
    if (learn.trainType === 'repeat') iconStatus = word.repeatAt ? iconList.wordReady : iconList.wordError;

    return `
      <button class="button button_word-full js-button-word" data-value="${word?.english}">
        <div class="button__caption">${word?.english || '-'}</div>
        <div class="button__value">${word?.native || '-'}</div>
        <span class="button__icon-wrapper">${iconStatus}</span> 
      </button>
    `;
  },

  actions(id) {
    switch(id) {
      case 'done':
        learn.endStage();
        break;
    }
  },
};
