const wordWrite = {
  words: [],
  readyWords: [],
  currentWord: null,
  inputWord: '',
  currentStage: 0,
  totalStages: 0,

  start(words, currentStage, totalStages) {
    current.page = 'wordWrite';
    this.currentStage = currentStage;
    this.totalStages = totalStages;

    // clearing
    this.readyWords.length = 0;
    this.currentWord = null;
    this.inputWord = '';

    // set new data
    this.words = words;

    this.nextStep();
  },

  nextStep() {
    // erase field
    this.inputWord = '';
    const field = document.getElementById('entered-word');
    if (field) {
      field.value = '';
    }

    if (this.words.length) {    
      this.currentWord = this.words[0];
      this.displayPage();
    
    } else {
      learn.endStage(this.readyWords.map(e => {
        // need erase translated field for next translate
        e.translated = false;
        return e;
      }));
    }
  },

  displayPage() {
    content.innerHTML = this.getTemplate();
  },

  getTemplate() {
    const stages = common.getStageTemplate(this.currentStage, this.totalStages);

    return `
      <div class="content__value">
        <div class="button-group button-group_navigation">
          <button class="button button_icon button_small both-mode button_without-shadow js-button" id="home">${iconList.close1}</button>
        </div>

        ${stages}

        <h2 class="header-2">${lang.word_write_header[current.lang]}</h2> 

        <div class="word">${this.currentWord.native}</div>
        <div class="custom-field__wrapper">
          <input type="text" class="custom-field js-field" placeholder="..." id="entered-word" autocomplete="off">
          <button class="button custom-field__delete button_icon button_without-shadow button_x-small js-button" id="erase-word">${iconList.delete}</button>
        </div>

        <!-- need for button placement -->
        <div class="content__letter-list"></div>

        <div class="button-group button-group_row" id="actions">
          <button class="button button_icon js-button" id="help">${iconList.question1}</button>
          <button disabled class="button button_icon js-button" id="next">${iconList.chevronRight}</button>
        </div>
      </div>
    `;
  },

  actions(id, value = '') {
    switch(id) {
      case 'home':
        if (window.confirm(lang.translate_words_exit[current.lang])) {
          home.displayPage();
        }
        break;
      case 'help':
        this.showRightAnswer(false);
        break;
      case 'next':
        this.nextStep();
        break;
      case 'erase-word':
        this.eraseWord();
        break;
      case 'entered-word':
        this.enterWord(value);
        break;
    }
  },

  enterWord(word) {
    this.inputWord = word;

    if (this.isWordRight()) {
      this.showRightAnswer(true);
    }
  },

  eraseWord() {
    this.inputWord = '';
    document.getElementById('entered-word').value = '';
  },

  isWordRight() {
    return this.inputWord.toLowerCase() === this.currentWord.english.toLowerCase();
  },

  showRightAnswer(isRight) {
    // change HELP button by NEXT button
    document.getElementById('help').setAttribute('disabled', 'true');
    document.getElementById('next').removeAttribute('disabled');

    // disable clear button
    document.getElementById('erase-word').classList.remove('js-button');

    // show right word value
    document.getElementById('entered-word').value = this.currentWord.english;

    // block input
    const field = document.getElementById('entered-word');
    field.setAttribute('readonly', 'true');
    field.classList.remove('js-field');

    if (isRight) {
      this.currentWord.translated = true; // update status - no more translate for it
      this.readyWords.push(this.currentWord);
      this.words.shift(); // remove word from words
    
    } else {
      this.currentWord.hasError = true; // has error for all train
      this.words.shift(); // remove word from start pos
      this.words.push(this.currentWord); // push it to end pos
    }
  },
};
