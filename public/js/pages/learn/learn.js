const learn = {
  words: [],
  // stages: ['selectWords', 'translateToNative', 'translateToEng', 'wordConstructor', 'wordWrite', 'learnResults'],
  stages: [],
  trainType: '',
  currentStage: '',
  
  startStage() {
    const currentStageNum = this.stages.indexOf(this.currentStage);

    switch (this.currentStage) {
      case 'selectWords': selectWords.start(currentStageNum, this.stages.length); break;
      case 'translateToNative': translateWords.start('toNative', this.words, currentStageNum, this.stages.length); break;
      case 'translateToEng': translateWords.start('toEn', this.words, currentStageNum, this.stages.length); break;
      case 'learnResults': learnResult.start(this.words, currentStageNum, this.stages.length); break;
      case 'wordConstructor': wordConstructor.start(this.words, currentStageNum, this.stages.length); break;
      case 'wordWrite': wordWrite.start(this.words, currentStageNum, this.stages.length); break;
    }
  },

  start(stageList) {
    this.trainType = stageList;

    switch (stageList) {
      case 'learn': 
        this.stages = [];
        this.stages.push('selectWords');
        if (current.settings.learn.toNative) this.stages.push('translateToNative');
        if (current.settings.learn.toEnglish) this.stages.push('translateToEng');
        if (current.settings.learn.constructor) this.stages.push('wordConstructor');
        if (current.settings.learn.writing) this.stages.push('wordWrite');

        this.stages.push('learnResults');
        break;
      case 'repeat':
        this.stages = [];
        if (current.settings.repeat.toNative) this.stages.push('translateToNative');
        if (current.settings.repeat.toEnglish) this.stages.push('translateToEng');
        this.stages.push('learnResults');

        let total = library.filter(e => e.learnedAt).sort(this.useSortDateOld);
        if (total.length > current.settings.repeatWords.current ) {
          total.length = current.settings.repeatWords.current;
        }
        this.setWords(total);
        break;
    }

    this.currentStage = this.stages[0];
    this.startStage()
  },

  useSortDateOld(a, b) {
    return (a.repeatAt || a.createdAt) > (b.repeatAt || b.createdAt) ? 0 : -1;
  },

  setWords(wordList) {
    this.words = wordList.map(word => {
      return {
        english: word.english,
        native: word.native,
        transcription: word.transcription || '',
        translated: false,
        hasError: false
      }
    });
  },

  endStage(words = []) {
    if (words.length) {
      this.words = words;
    }
    this.nextStage();
  },

  nextStage() {
    const stageNumber = this.stages.indexOf(this.currentStage);

    if (stageNumber < this.stages.length -1) {
      this.currentStage = this.stages[stageNumber + 1];
      this.startStage()
    
    } else {
      // save result
      current.saveDb();

      // end learn
      home.displayPage();
    }
  },
};
