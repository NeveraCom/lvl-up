const settings = {
  displayPage() {
    current.page = 'settings';
    content.innerHTML = this.getTemplate();
  },

  /**
   * @param {number} number
   * @returns {string}
   */
  getRangeLegend(number) {
    return new Array(number).fill(1).reduce((sum, current, i) => sum += `<span>${i + 1}</span>`, '');
  },

  getToggleMarkup(title, param, checked = false, separate = false) {
    const separatorClass = separate ? 'settings__block_separator' : '';
    return `
      <div class="settings__block ${separatorClass}">
        <div class="settings__row">
          <p class="settings__title">${ translate(title) }</p>
            <input 
              ${checked ? 'checked' : ''} 
              id="${param}" type="checkbox"
              onclick="settings.changeState('${param}')"
              class="togglebtn" autocomplete="off" 
             />
            <label for="${param}" class="togglebtn">
              <span class="indicator"></span>
            </label>
        </div>
      </div>
    `;
  },

  getRangeMarkup(title, param, min, max, value, separate = false) {
    const separatorClass = separate ? 'settings__block_separator' : '';
    return `
      <div class="settings__block ${separatorClass}">
        <p class="settings__title">${ translate(title) }</p>
        <div class="settings__row">
          <input 
            onchange="settings.changeState('${param}', value)"
            class="settings__range"
            type="range" name="volume" 
            min="${min}" max="${max}" value="${value}"
           >
          <div id="${ param }" class="settings__value">${value}</div>
        </div>
      </div>
    `;
  },

  changeState(param, value) {
    switch (param) {
      case 'learnToNative': current.settings.learn.toNative = !current.settings.learn.toNative; break;
      case 'learnToEnglish': current.settings.learn.toEnglish = !current.settings.learn.toEnglish; break;
      case 'learnConstructor': current.settings.learn.constructor = !current.settings.learn.constructor; break;
      case 'learnWriting': current.settings.learn.writing = !current.settings.learn.writing; break;
      case 'repeatToNative': current.settings.repeat.toNative = !current.settings.repeat.toNative; break;
      case 'repeatToEnglish': current.settings.repeat.toEnglish = !current.settings.repeat.toEnglish; break;
      case 'learnRangeWords':
        current.settings.learnWords.current = value;
        const valueLearn = document.getElementById('learnRangeWords');
        valueLearn ? valueLearn.innerText = value : null;
        break;
      case 'repeatRangeWords':
        current.settings.repeatWords.current = value;
        const valueRepeat = document.getElementById('repeatRangeWords');
        valueRepeat ? valueRepeat.innerText = value : null;
        break;
      case 'learnConstructorAddLettersToggle': current.settings.constructorAddLetters.state = !current.settings.constructorAddLetters.state; break;
      case 'learnConstructorAddLetters':
        current.settings.constructorAddLetters.current = value;
        const valueLetters = document.getElementById('learnConstructorAddLetters');
        valueLetters ? valueLetters.innerText = value : null;
        break;
    }

    current.saveSettings();
  },

  getTemplate() {
    return `
      <div class="content__value">
        <div class="button-group button-group_navigation">
          <button class="button button_icon button_small both-mode button_without-shadow js-button" id="settings-home">${iconList.arrowLeft}</button>
        </div>
        
        <h2 class="header-2">${ translate('SETTINGS.HEADER_MAIN') }</h2>
        <h3 class="header-3">${ translate('SETTINGS.HEADER_COMMON') }</h3>

        <div class="content__home-actions">
          <input type="checkbox" id="lang-switcher" class="js-checkbox" ${current.englishInterface ? "checked" : ""}>
          <label for="lang-switcher" class="custom-checkbox button button_icon">
            <div class="custom-checkbox__content">
              <div class="custom-checkbox__content-icon">${iconList.ua}</div>
              <div class="custom-checkbox__content-icon">${iconList.en}</div>
            </div>
          </label>

          <input type="checkbox" id="mode-switcher" class="js-checkbox" ${current.darkMode ? "checked" : ""}>
          <label for="mode-switcher" class="custom-checkbox button button_icon">
            <div class="custom-checkbox__content">${iconList.sun}${iconList.moon}</div>
          </label>
        </div>
        
        <h3 class="header-3">${ translate('SETTINGS.HEADER_LEARN') }</h3>
        
        ${this.getRangeMarkup(
          'SETTINGS.LEARN_WORDS', 
          'learnRangeWords', 
          current.settings.learnWords.min, 
          current.settings.learnWords.max, 
          current.settings.learnWords.current
        )}
        
        ${this.getToggleMarkup('SETTINGS.OPTION_LEARN_STAGE_TO_NATIVE', 'learnToNative', current.settings.learn.toNative)}
        ${this.getToggleMarkup('SETTINGS.OPTION_LEARN_STAGE_TO_ENGLISH', 'learnToEnglish', current.settings.learn.toEnglish)}
        ${this.getToggleMarkup('SETTINGS.OPTION_LEARN_STAGE_CONSTRUCTOR', 'learnConstructor', current.settings.learn.constructor)}
        ${this.getToggleMarkup('SETTINGS.OPTION_LEARN_STAGE_WRITING', 'learnWriting', current.settings.learn.writing)}
        
        ${this.getToggleMarkup('SETTINGS.OPTION_LEARN_CONSTRUCTOR_ADD_LETTERS', 'learnConstructorAddLettersToggle', current.settings.constructorAddLetters.state, true)}
        ${this.getRangeMarkup(
          'SETTINGS.OPTION_LEARN_CONSTRUCTOR_ADD_LETTERS_COUNT',
          'learnConstructorAddLetters',
          current.settings.constructorAddLetters.min,
          current.settings.constructorAddLetters.max,
          current.settings.constructorAddLetters.current
        )}
        
        <h3 class="header-3">${ translate('SETTINGS.HEADER_REPEAT') }</h3>
        
        ${this.getRangeMarkup(
          'SETTINGS.REPEAT_WORDS',
          'repeatRangeWords',
          current.settings.repeatWords.min,
          current.settings.repeatWords.max,
          current.settings.repeatWords.current
        )}
        
        ${this.getToggleMarkup('SETTINGS.OPTION_LEARN_STAGE_TO_NATIVE', 'repeatToNative', current.settings.repeat.toNative)}
        ${this.getToggleMarkup('SETTINGS.OPTION_LEARN_STAGE_TO_ENGLISH', 'repeatToEnglish', current.settings.repeat.toEnglish)}

        <h3 class="header-3">${ translate('SETTINGS.HEADER_SAVING') }</h3>
        
        <details class="settings__details">
          <summary>${translate('SETTINGS.HOW_SAVING')}</summary>
          <p class="text">${lang.settings_text1[current.lang]}</p>
          <p class="text">${lang.settings_text2[current.lang]}</p>
          <textarea class="textarea textarea_invisible" id="words_db"></textarea>
        </details>
        <button class="button button_margin button_small js-button" id="settings-copy">${lang.button_copy[current.lang]}</button>
        
        <details class="settings__details">
          <summary>${ translate('SETTINGS.ABOUT_CLEARING_DB') }</summary>
          <p class="text">${lang.settings_text3[current.lang]}</p>
        </details>
        <button class="button button_margin button_small js-button" id="db-clear">${lang.button_clear[current.lang]}</button>
        
        <p class="text">${ translate('SETTINGS_MESSAGE.DELETE_ALL') }</p>
        <button class="button button_margin button_small js-button" id="settings-clear">${ translate('BUTTONS.DELETE_ALL') }</button>
      </div>
    `;
  },

  actions(id, value = null) {
    switch(id) {
      case 'settings-home':
        home.displayPage();
        break;
      case 'lang-switcher':
        current.switchLang();
        this.displayPage();
        break;
      case 'mode-switcher':
        current.switchDarkMode();
        break;
      case 'settings-copy':
        this.copyDB();
        break;
      case 'db-clear':
        this.clearDB();
        break;
      case 'settings-clear':
        this.clearLocalStorage();
        break;
    }
  },

  clearDB() {
    if (window.confirm(lang.clearDB[current.lang])) {
      try {
        localStorage.removeItem('library');
        toast.show(lang.DBClearedSuccess[current.lang])
      } catch(e) {
        toast.show(lang.DBClearedError[current.lang], 'warning');
        console.error('Error: ', e);
      }
    }
  },

  clearLocalStorage() {
    if (window.confirm(lang.clearDB[current.lang])) {
      try {
        localStorage.clear();
        toast.show(lang.DBClearedSuccess[current.lang])
      } catch(e) {
        toast.show(lang.DBClearedError[current.lang], 'warning');
        console.error('Error: ', e);
      }
    }
  },

  copyDB() {
    const textArea = document.getElementById('words_db');
    textArea.value = 'let library = ' + JSON.stringify(library, ' ') + ';';
    textArea.focus();
    textArea.select();

    try {
      const successful = document.execCommand('copy');
      toast.show( 
        successful ? lang.DBCopiedSuccess[current.lang] : lang.DBCopiedError[current.lang],
        successful ? 'primary' : 'warning'
      );
      console.log(successful);
    } catch (e) {
      toast.show(lang.DBCopiedError[current.lang], 'warning');
      console.error('Error: ', e)
    }
  },
}
