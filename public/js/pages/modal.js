const modal = {
  displayed: true,
  newWord: true,
  changedWordIndex: -1,

  word: {},

  wordTemplate: {
    english: '',
    transcription: '',
    native: '',
    createdAt: null,
    learnedAt: null,
    repeatAt: null,
    ignored: false
  },

  displayModal(value = '') {
    const searchedWord = value ? library?.find(e => e.english === value) : '';
    this.newWord = ( value && searchedWord ) ? false : true;

    if (this.newWord) {
      this.changedWordIndex = -1;
      this.word = Object.assign(this.word, this.wordTemplate)

    } else {
      this.changedWordIndex = library.map(e => e.english).indexOf(value);
      searchedWord.transcription = searchedWord.transcription ?? null;
      this.word = Object.assign(this.word, searchedWord)
    }

    this.displayed = true;
    modalContent.innerHTML = this.getTemplate();
    modalWrapper.classList.add('modal__wrapper_show');
  },

  getTemplate() {
    const transcription = this.word?.transcription || '';

    return `
      <div class="custom-field__wrapper">
        <input type="text" class="custom-field custom-field_small js-field" placeholder="${lang.enterWord[current.lang]}" value="${this.word.english}" id="modal-word">
        <button class="button custom-field__delete button_icon button_without-shadow button_x-small js-button" id="modal-word-clear">${iconList.delete}</button>
      </div>

      <div class="custom-field__wrapper">
        <input type="text" class="custom-field custom-field_small js-field" placeholder="${lang.enterTranscription[current.lang]}" value="${transcription}" id="modal-transcription">
        <button class="button custom-field__delete button_icon button_without-shadow button_x-small js-button" id="modal-transcription-clear">${iconList.delete}</button>
      </div>

      <div class="custom-field__wrapper">
        <textarea class="custom-field custom-field_small textarea js-field" placeholder="${lang.enterTranslate[current.lang]}" id="modal-translate">${this.word.native}</textarea>
        <button class="button custom-field__delete button_icon button_without-shadow button_x-small js-button" id="modal-translate-clear">${iconList.delete}</button>
      </div>

      <div class="button-group button-group_gap-2" id="modal-state">
        <button class="button button_small js-button ${this.word.learnedAt ? 'button_active' : ''}" id="modal-status-learned" data-value="learned">${lang.learnedOne[current.lang]}</button>
        <button class="button button_small js-button ${this.word.ignored ? 'button_active' : ''}" id="modal-status-ignored" data-value="ignored">${lang.ignoredOne[current.lang]}</button>
      </div>

      <hr class="hr">

      <div class="button-group button-group_row">
        <button class="button button_icon js-button" id="modal-delete" data-value="delete">${iconList.deleteItem}</button>
        <button class="button button_icon js-button" id="modal-cancel" data-value="new">${iconList.cancel}</button>
        <button class="button button_icon js-button" id="modal-apply" data-value="learned">${iconList.ok}</button>
      </div>
    `;
  },

  hideModal() {
    modalWrapper.classList.remove('modal__wrapper_show');
    this.displayed = false;
  },

  statusLearnedUpdate() {
    const btn = document.getElementById('modal-status-learned');

    if (this.word.learnedAt) {
      this.word.learnedAt = null;
      btn.classList.remove('button_active');
    } else {
      this.word.learnedAt = Date.now();
      btn.classList.add('button_active');
    }

    this.word.repeatAt = null;
  },

  statusIgnoredUpdate() {
    const btn = document.getElementById('modal-status-ignored');

    if (this.word.ignored) {
      this.word.ignored = false;
      btn.classList.remove('button_active');
    } else {
      this.word.ignored = true;
      btn.classList.add('button_active');
    }
  },

  clearWordValue() {
    this.word.english = '';
    document.getElementById('modal-word').value = '';
  },

  clearTranscriptionValue() {
    this.word.transcription = '';
    document.getElementById('modal-transcription').value = '';
  },

  clearWordTranslate() {
    this.word.native = '';
    document.getElementById('modal-translate').value = '';
  },

  wordUpdate() {
    if (!this.word.english || !this.word.native) return;

    this.word.english = this.word.english.trim();
    this.word.transcription = this.word.transcription?.trim();
    this.word.native = this.word.native.trim();

    if (this.newWord) {
      this.word.createdAt = Date.now();
      library.push(Object.assign({}, this.word));
      this.hideModal();

    } else {
      if (this.changedWordIndex >= 0) {
        library.splice(this.changedWordIndex, 1, Object.assign({}, this.word));
      }
    }

    current.saveDb();

    this.hideModal();
      switch(current.page) {
        case 'words': words.displayWords(); break;
        case 'learnResult': learnResult.updatedDisplayWords(); break;
      }
  },

  deleteWord() {
    if (this.newWord) return;
    if (window.confirm(lang.deleteWordMessage[current.lang])) {
      if (this.changedWordIndex >= 0) {
        library.splice(this.changedWordIndex, 1);
      }
    }

    current.saveDb();

    this.hideModal();
    if (current.page === 'words') {
      words.displayWords();
    }
  },
};


const modalWrapper = document.getElementById('modal-wrapper');
modalWrapper.addEventListener('click', () => {
  modal.hideModal();
})


const modalContent = document.getElementById('modal-content');
modalContent.addEventListener('click', (e) => {
  e.stopPropagation();

  const btn = e.target.closest('.js-button');
  const btnAttr = btn?.getAttribute('id');
  if (!btn || !btnAttr) return;

  switch (btn.getAttribute('id')) {
    case 'modal-delete': modal.deleteWord(); break;
    case 'modal-cancel': modal.hideModal(); break;
    case 'modal-apply': modal.wordUpdate(); break;
    case 'modal-status-learned': modal.statusLearnedUpdate(); break;
    case 'modal-status-ignored': modal.statusIgnoredUpdate(); break;
    case 'modal-word-clear': modal.clearWordValue(); break;
    case 'modal-transcription-clear': modal.clearTranscriptionValue(); break;
    case 'modal-translate-clear': modal.clearWordTranslate(); break;
  }
});

modalContent.addEventListener('keyup', (e) => {
  const field = e.target.closest('.js-field');
  if (!field) return;

  switch (field.getAttribute('id')) {
    case 'modal-word': modal.word.english = e.target.value; break;
    case 'modal-transcription': modal.word.transcription = e.target.value; break;
    case 'modal-translate': modal.word.native = e.target.value; break;
  }
});
