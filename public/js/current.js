const current = {
  // home, settings, words, stage-to-eng, stage-to-native, stage-constructor, stage-write, ...
  maxWordsNumberInTrain: 10,
  appVersion: '0.5',

  settings: {
    version: '0.5',
    page: 'home',
    darkMode: false,
    constructorAddLetters: {
      state: false,
      min: 1,
      max: 8,
      current: 4
    },
    learnWords: {
      min: 1,
      max: 8,
      current: 4
    },
    repeatWords: {
      min: 1,
      max: 20,
      current: 10
    },
    learn: {
      toNative: true,
      toEnglish: true,
      constructor: true,
      writing: true
    },
    repeat: {
      toNative: true,
      toEnglish: true,
    }
  },

  get lang() {
    return  this.englishInterface ? 'en' : 'ua';
  },

  changeCurrentPage(page) {
    this.page = page;
  },

  switchLang() {
    this.englishInterface = !this.englishInterface;
    this.saveSettings();
  },

  switchDarkMode() {
    this.darkMode = !this.darkMode;
    this.darkMode ? body.classList.add('dark-mode') : body.classList.remove('dark-mode');
    this.saveSettings();
  },

  enableDarkMode() {
    this.darkMode = true;
    body.classList.add('dark-mode');
  },

  // LOCAL STORAGE
  // save current base to localStorage
  saveDb() {
    localStorage.setItem('library', JSON.stringify(library, null, ' '));
  },

  clearDb() {
    localStorage.removeItem('library');
  },

  loadDb() {
    const local = JSON.parse(localStorage.getItem('library')) || [];

    library = [...local, ...library.filter(libItem => !local.some(localItem => localItem.english === libItem.english))];
  },

  saveSettings() {
    localStorage.setItem('settings', JSON.stringify(this.settings));
  },

  loadSettings() {
    const loadedSettings = JSON.parse(localStorage.getItem('settings'));

    if (loadedSettings) {
      this.settings = loadedSettings;
    }

    if (this.settings.darkMode) {
      this.enableDarkMode();
    }
  },

  /**
   * check version
   * @returns {boolean}
   */
  checkVersion() {
    const localSettings = JSON.parse(window.localStorage.getItem('settings'));

    if (current.appVersion === localSettings?.version) {
      return true;

    // if another version
    } else if (localSettings?.version) {
      toast.show(translate('ERROR_MESSAGES.WRONG_VERSION'), 'warning', 10000);
    }

    // clear
    window.localStorage.removeItem('settings');
  },

  // START
  start() {
    this.loadDb();
    this.loadSettings();
    this.checkVersion();

    home.displayPage();

    // add icon in markup (toast - close button)
    document.querySelector('#toast-close-button').innerHTML = iconList.delete;
    // add png logo
    document.querySelector('#header').innerHTML = `<img src="${iconList.logo}" alt="logo">`;
  },
  
};

const body = document.querySelector('body');
const content = document.querySelector('.content');
