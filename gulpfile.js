const { src, dest, parallel, series, watch } = require('gulp');

// browserSync
const browserSync = require('browser-sync').create();
// js
const concat = require('gulp-concat');
const uglify = require('gulp-uglify-es').default;
// styles
const sass = require('gulp-sass')(require('sass'));
const cleancss = require('gulp-clean-css');


function browsersync() {
  browserSync.init({
    server: { baseDir: 'dist/' },
    notify: false,
    online: true
  })
}

function scripts() {
  return src([
      'public/js/current.js', // main state
      'public/js/services/*.js', // services
      'public/js/services/*.js', // services
      'public/js/content-actions.js', // actions
      'public/js/pages/*.js', // pages
      'public/js/pages/learn/*.js', // stages of learn
    ])
    .pipe(concat('script.min.js'))
    .pipe(uglify())
    .pipe(dest('./dist/js/'))
    .pipe(browserSync.stream())
}

function startwatch() {
  watch(['public/js/**/*.js'], scripts).on('change', browserSync.reload);
  watch(['public/scss/**/*.scss', 'public/css/**/*.css'], styles);
  watch(['public/index.html'], buildcopy).on('change', browserSync.reload);
}

function styles() {
  return src(['public/scss/**/*.scss', 'public/css/**/*.css'])
    .pipe(sass())
    .pipe(concat('styles.min.css')) // Конкатенируем в файл app.min.css
    .pipe(cleancss( { level: { 1: { specialComments: 0 } }/* , format: 'beautify' */ } )) // минификация
    .pipe(dest('./dist/css/'))
    .pipe(browserSync.stream())
}

function buildcopy() {
  return src(['public/index.html', 'public/db/words.js'], { base: 'public' }) // Параметр "base" сохраняет структуру проекта при копировании
    .pipe(dest('dist')) // Выгружаем в папку с финальной сборкой
    .pipe(browserSync.stream())
}

exports.startwatch = startwatch;
exports.scripts = scripts;
exports.browsersync = browsersync;
exports.styles = styles;
exports.buildcopy = buildcopy;
exports.build = series(styles, scripts, buildcopy);
exports.default = parallel(browsersync, startwatch);
